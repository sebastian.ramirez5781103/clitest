import Generator from 'yeoman-generator';
import chalk from "chalk";

const { green, magenta, blue} = chalk;
export default class extends Generator {

  constructor(args, opts) {
    super(args, opts);
    this.argument('appname', { type: String, required: false });
  }

  // Async Await
  async prompting() {
    this.answers = await this.prompt([{
      type: 'input',
      name: 'name',
      message: 'Your project name',
      default: this.appname, // appname return the default folder name to project
      store: true,
    },
    {
      type: 'list',
      name: 'templateType',
      message: 'Select the template wanted:',
      choices: ['Front-End', 'Node API builder', 'FullStack Application']
    },
    {
      type: 'list',
      name: 'frontTemplateType',
      message: 'Select the front-end framework:',
      choices: ['React', 'Next.js'],
      when: answers => answers.templateType === 'Front-End'
    }]
    ) 
  }

  writing() {
    if (this.answers.templateType === 'Front-End' && this.answers.frontTemplateType === 'React') {
      if (this.answers.frontTemplateType === 'React') {
        this._writingReactTemplate();
      } else if (this.answers.frontTemplateType === 'Next.js' && this.answers.frontTemplateType === 'Next.js') {
        this._writingNextTemplate();
      }
    } else if (this.answers.templateType === 'Node API builder') {
      this._writingApiTemplate();
    } else {
      this._writingReactTemplate();
       this._writingApiTemplate();
    }
  }

  _writingReactTemplate() {
    this.fs.copy(
      this.templatePath('frontend/public'),
      this.destinationPath('frontend/public')
    )
    this.fs.copyTpl(
      this.templatePath('frontend/public'),
      this.destinationPath('frontend/public'),
      { title: this.answers.name } // Embedded JavaScript templating.

    )
  }
  _writingNextTemplate() {
    this.fs.copy(
      this.templatePath('frontend/next'),
      this.destinationPath('frontend/next')
    )
    this.fs.copyTpl(
      this.templatePath('frontend/next'),
      this.destinationPath('frontend/next'),
      { title: this.answers.name } // Embedded JavaScript templating.

    )
  }

  _writingApiTemplate() {
    this.fs.copy(
      this.templatePath('api'),
      this.destinationPath('api')
    )
  }

  end() {
    this.log(green('------------'))
    this.log(magenta('***---***'))
    this.log(blue('Jobs is Done!'))
    this.log(green('------------'))
    this.log(magenta('***---***'))
  }
};